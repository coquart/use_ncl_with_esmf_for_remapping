; Written by L. Coquart and M. Haley, 13/01/2017
; This script regrids data from a curvilinear grid to a
; curvilinear grid. Both initial grids are in one NetCDF file.
;--------------------------------------------
;---- Predefined ESMF modules
;--------------------------------------------
load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/gsn_code.ncl"
load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/gsn_csm.ncl"
load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/contributed.ncl"
load "$NCARG_ROOT/lib/ncarg/nclscripts/esmf/ESMF_regridding.ncl"

begin

;--------------------------------------------
;--- PATHS
;--------------------------------------------
;--- RUNDIR : created in the script script_interp_ana_with_esmf_NCL_CURV_to_CURV_grids.sh 
  RUNDIR="wkdir_ana_esmf_NCL_"+str_lower(srcgrid)+"_"+str_lower(tgtgrid)+"_"+ str_lower(interp_method)+"_ocean/"

;--------------------------------------------
;--- Source and Target grid Files used with oasis3-mct for SCRIP
;--------------------------------------------
  gridsfile = "grid_msk_area_"+srcgrid+".nc"    ; File with grids, areas and masks at OASIS3-MCT description with undescore
  gridtfile = "grid_msk_area_"+tgtgrid+".nc"    ; File with grids, areas and masks at OASIS3-MCT description with undescore

;--------------------------------------------
;--- Files 
;--------------------------------------------  
  funcini            = datafile                    ; File with the initial field
  funcinterp         = "fldou_interp_ocean.nc"      ; File with the remapping field when masking land
  masktgtinterp      = "destination_grid_file.nc"   ; File with target grid at ESMF format and the mask
  srcGridName        = "source_grid_file.nc"        ; File with target grid at ESMF format
  tgtGridName        = "destination_grid_file.nc"   ; File with target grid at ESMF format
; Weight file obtained where masked target points and target points do not receive any value
  wgtFile            = "rmp_"+srcgrid+"to"+tgtgrid+"_" + interp_method + "_withfillValue_ocean.nc"
; Weight file obtained for the neareststod between the source and the target grids
  wgtFile1           = "rmp_"+srcgrid+"to"+tgtgrid+"_neareststod_ocean.nc"
; Weight file name if remapping asked by the user is neareststod
  wgtFilennei        = "rmp_"+srcgrid+"to"+tgtgrid+"_" + interp_method + "_ocean.nc"

;-------------------------------------------------------
;--- Read the funtion to interpolate in datafile  
;-------------------------------------------------------

  ifile = addfile(funcini,"r")
  field_in = ifile->$varin$(:,::-1,:)

  ntime=dimsizes(field_in(:,0,0))

;--------------------------------------------
;--- Constants
;-------------------------------------------- 
  msk_field_value = 10000
  msk_err_value = -10000
  echelle = 30
  msk_zero=0
  fld_zero=9.96921e+36
  weight_fill_val = -50000
  pi = 3.14159265359
  dp_length = 1.2*pi
  dp_conv   = pi/180.

;--------------------------------------------
;--- Coordinate names
;--------------------------------------------
  srcgrdlon = srcgrid+"_lon"
  srcgrdlat = srcgrid+"_lat"
  srcgrdclo = srcgrid+"_clo"
  srcgrdcla = srcgrid+"_cla"
  srcgrdmsk = srcgrid+"_msk"
  tgtgrdlon = tgtgrid+"_lon"
  tgtgrdlat = tgtgrid+"_lat"
  tgtgrdclo = tgtgrid+"_clo"
  tgtgrdcla = tgtgrid+"_cla"
  tgtgrdmsk = tgtgrid+"_msk"

;--------------------------------------------
;--- Read grids, masks, areas arrays
;--------------------------------------------
  gfiles   = addfile(gridsfile,"r")
  src_lon  = gfiles->$srcgrdlon$
  src_lat  = gfiles->$srcgrdlat$
  src_msk  = gfiles->$srcgrdmsk$

  gfilet   = addfile(gridtfile,"r")
  tgt_lon  = gfilet->$tgtgrdlon$
  tgt_lat  = gfilet->$tgtgrdlat$
  tgt_msk  = gfilet->$tgtgrdmsk$

  nlatsrc=dimsizes(src_lat(:,0))
  nlonsrc=dimsizes(src_lon(0,:))
  nlattgt=dimsizes(tgt_lat(:,0))
  nlontgt=dimsizes(tgt_lon(0,:))

;----------------------------------------------------------------------
;--- Regridding all in one step for interp_method 
;--- Weight file exists
  if (JOBO .eq. "use_wgt") then
     system("/bin/rm -f "+funcini)
     system("/bin/rm -f "+funcinterp)
     Opt            = True
     if (interp_method .eq. str_lower("neareststod")) then
        func_regrid = ESMF_regrid_with_weights(field_in,wgtFilennei,Opt)
     else
        func_regrid = ESMF_regrid_with_weights(field_in,wgtFile,Opt)
     end if
  else
;--- Weight file does not exit 
;--- Cleaning: remove any pre-existing file
;-------------------------------------------- 
  system("/bin/rm -f "+funcinterp)
  system("/bin/rm -f "+srcGridName)
  system("/bin/rm -f "+tgtGridName)
  system("/bin/rm -f "+wgtFile)
  system("/bin/rm -f "+wgtFile1)
  system("/bin/rm -f "+wgtFilennei)
  system("/bin/rm -f PET0.RegridWeightGen.Log")

  Opt             = True
  Opt@SrcGridName = srcGridName 
  Opt@SrcGridLat  = src_lat
  Opt@SrcGridLon  = src_lon
  Opt@SrcMask2D   = 1-src_msk
  Opt@DstGridName = tgtGridName
  Opt@DstGridLat  = tgt_lat
  Opt@DstGridLon  = tgt_lon
  Opt@DstMask2D   = 1-tgt_msk
  if (srcperiodic .eq. str_lower("region")) then
     Opt@SrcRegional  = True
  end if
  if (tgtperiodic .eq. str_lower("region")) then
     Opt@DstRegional  = True
  end if

  Opt@InterpMethod   = str_lower(interp_method)
  if (interp_method .eq. str_lower("neareststod")) then
     Opt@WgtFileName    = wgtFilennei
  else
     Opt@WgtFileName    = wgtFile
  end if

;  Opt@Pole           = "none"
  Opt@ForceOverwrite = True
  Opt@PrintTimings   = True
  Opt@Debug          = True

  if ( interp_method .eq. str_lower("conserve") ) then
    lat_name  = "y_" + srcgrid
    lon_name  = "x_" + srcgrid
    size_name = "crn_" + srcgrid
;---Make sure grid is nlat x nlon x 4
    Opt@SrcGridCornerLat  = gfiles->$srcgrdcla$($lat_name$|:,$lon_name$|:,$size_name$|:)     
    Opt@SrcGridCornerLon  = gfiles->$srcgrdclo$($lat_name$|:,$lon_name$|:,$size_name$|:)
    printMinMax(Opt@SrcGridCornerLat,0)
    printMinMax(Opt@SrcGridCornerLon,0)
  end if	 

  if ( interp_method .eq. str_lower("conserve") ) then
    lat_name  = "y_" + tgtgrid
    lon_name  = "x_" + tgtgrid
    size_name = "crn_" + tgtgrid
;---Make sure grid is nlat x nlon x 4
    Opt@DstGridCornerLat  = gfilet->$tgtgrdcla$($lat_name$|:,$lon_name$|:,$size_name$|:)
    Opt@DstGridCornerLon  = gfilet->$tgtgrdclo$($lat_name$|:,$lon_name$|:,$size_name$|:)
    printMinMax(Opt@DstGridCornerLat,0)
    printMinMax(Opt@DstGridCornerLon,0)
  end if	 

;---You can choose to skip any of these three steps.
; Opt@SkipSrcGrid   = True    ; Will assume source grid file has been generated
; Opt@SkipDstGrid   = True    ; Will assume target grid file has been generated
; Opt@SkipWgtGen    = True    ; Will assume weights file has been generated
  
;++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
; Points that receive no values are at _FillValue=9.96920996838687e+36 (blank points)
;=============================================
    func_regrid = ESMF_regrid(field_in,Opt)
  end if
;=============================================
  delete(gfiles)
  delete(gfilet)
  printVarSummary(func_regrid)
  undef_val=func_regrid@_FillValue
  printVarSummary(undef_val)
  delete (Opt)
  minterpfile   = addfile(masktgtinterp,"r")
  tgt_msk_1d  = minterpfile->$"grid_imask"$
  tgt_msk_nd = onedtond(tgt_msk_1d,(/nlattgt,nlontgt/))
  tgt_msk_interp=new((/ntime,nlattgt,nlontgt/),"integer")
  tgt_msk_interp(0,:,:)=tgt_msk_nd(:,:)
  do n=1,ntime-1
  tgt_msk_interp(n,:,:)=tgt_msk_interp(0,:,:)
  end do
  printVarSummary(tgt_msk_interp)
;============================================================================================
  func_regrid_msk_fillvalue  = where((tgt_msk_interp) .eq. 1, func_regrid, msk_field_value)
;============================================================================================
;
;++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
  if (interp_method .eq. str_lower("neareststod")) then
     print("Nothing to do")
     func_regrid_new = func_regrid
     func_regrid_msk_new = func_regrid_msk_fillvalue
  else
;--------------------------------------------------------------------------------------------
;   Do the regridding "neareststod" all in one step to fill blank points of interp_method
;--------------------------------------------------------------------------------------------
  if (JOBO1 .eq. "use_wgt1") then
     Opt            = True
     func_regrid_nn = ESMF_regrid_with_weights(field_in,wgtFile1,Opt)
  else
  Opt=True
  Opt@SrcGridName    = srcGridName 
  Opt@DstGridName    = tgtGridName
  Opt@WgtFileName    = wgtFile1
  Opt@SrcGridLat     = src_lat
  Opt@SrcGridLon     = src_lon
  Opt@DstGridLat     = tgt_lat
  Opt@DstGridLon     = tgt_lon
  Opt@DstMask2D      = 1-tgt_msk
  Opt@SrcMask2D      = 1-src_msk

  if (srcperiodic .eq. str_lower("region")) then
     Opt@SrcRegional  = True
  end if
  if (tgtperiodic .eq. str_lower("region")) then
     Opt@DstRegional  = True
  end if

  Opt@InterpMethod   = "neareststod"
;  Opt@Pole           = "none"
  Opt@ForceOverwrite = True
  Opt@PrintTimings   = True
  Opt@Debug          = True

;---You can choose to skip any of these three steps.
;  Opt@SkipSrcGrid   = True    ; Will assume source grid file has been generated
;  Opt@SkipDstGrid   = True    ; Will assume target grid file has been generated
;  Opt@SkipWgtGen    = True    ; Will assume weights file has been generated

;=================================================
    func_regrid_nn = ESMF_regrid(field_in,Opt)
  end if
;=================================================
;  print("func_regrid_nn :"+func_regrid_nn)
  func_neareststod_msk_fillvalue = where((tgt_msk_interp) .eq. 1, func_regrid_nn, msk_field_value)
;  print("func_neareststod_msk_fillvalue :"+func_neareststod_msk_fillvalue)

;----------------------------------------------------------------------
;--- Combine interp_method + neareststod
;----------------------------------------------------------------------
;================================================================================================================
  func_regrid_msk_new  = where(ismissing(func_regrid_msk_fillvalue), func_regrid_nn, func_regrid_msk_fillvalue)
;================================================================================================================
  end if

;--------------------------------------------
;--- Output files: to plot ESMF results for analytical functions
;--------------------------------------------

; To do the addition with the land
  func_regrid_msk_new = where((tgt_msk_interp) .eq. 1, func_regrid_msk_new, fld_zero)
  copy_VarCoords(func_regrid,func_regrid_msk_new)


  if (interp_method .eq. str_lower("neareststod")) then
     ofile = addfile(funcinterp,"c")
     ofile->$"tgtgrdlon"$=tgt_lon
     ofile->$"tgtgrdlat"$=tgt_lat
     ofile->$"field_interp_undef"$=(func_regrid)
     ofile->$"field_interp_msk_undef"$=(func_regrid_msk_fillvalue)
     ofile->$"field_ou_interp"$=(func_regrid_msk_new)
  else
      ofile = addfile(funcinterp,"c")
      ofile->$"tgtgrdlon"$=tgt_lon
      ofile->$"tgtgrdlat"$=tgt_lat
      ofile->$"func_neareststod"$=(func_neareststod_msk_fillvalue)
      ofile->$"field_interp_undef"$=(func_regrid)
      ofile->$"field_interp_msk_undef"$=(func_regrid_msk_fillvalue)
      ofile->$"field_ou_interp"$=(func_regrid_msk_new)
  end if

end
