#!/bin/ksh
#
# Script launching NCL programs to calculate the ramapping of an analytical function in one
# step with ESMF, using nearest option for target points that do not receive any value,
# to verify the quality of interpolation 
# Written by L. Coquart 01/08/2022
#
############################# USER SECTION ########################################################
## GRIDS: used in the name of the name of the working directory
# Name of the grids : OASIS nomenclature
# grid and mask : 4 characters for the name, and then _lon, _lat, _clo, _cla, _msk
# grid in double precision, mask as an integer
# By default in OASIS : mask = 1 on land and 0 on ocean when ocean is not masked
# ESMF used inversed convention. So when remapping from ocean to ocean ESMF uses
# 1-mask
#++++++++++++++++++++++++++++++++
## INTERPOLATION: bilinear, conserve, patch (2nd order), neareststod
# Remapping (done with ESMF in an NCL program see below), used in 
# the name of the working directory
# Corners needed for conserv interpolation
#++++++++++++++++++++++++++++++++
# Remap with land masked by default
# If land_interp=1 remap also from land to land with ocean masked
# and combined fields at the end with cdo
######################################################################
HOMEDIR=`pwd`
land_interp=0
srcgrid=nemo
tgtgrid=areg
# Are the weights already calculated for ocean ? (put get_wgt and/or get_wget1 if not else use_wgt and use_wgt1)
JOBO="get_wgt"
JOBO1="get_wgt1"
# Are the weights already calculated for land ? (put get_wgt and/or get_wget1 if not else use_wgt and use_wgt1)
JOBL="get_wgt"
JOBL1="get_wgt1"
# Are the grids regional (region) or periodical (global) ?
srcperiodic=global
tgtperiodic=global
interp_method=bilinear
#interp_method=conserve
#interp_method=patch
#interp_method=neareststod
#var_inO=tos
# For sst read data in interp_esmf_CURV_to_CURV_one_step_ocean.ncl using:
#   ifile = addfile(funcini,"r")
#   field_in = ifile->$varin$(:,::-1,:)
var_inO=siconc
#datafileO=tos_Omon_CNRM-CM6-1_pa-pdSIC-ext_r1i1p1f2_gn_200001-209912.nc
datafileO=siconc_SImon_CERFACS-EPOC-LR_EPOC-control-1950_r1i1p1f1_gn_195001-230012.nc
var_inL=tos
datafileL=tos_Omon_CNRM-CM6-1_pa-pdSIC-ext_r1i1p1f2_gn_200001-209912.nc
# Localisation of the grids
GRIDDIR=/data/scratch/globc/$USER/use_ncl_with_esmf_for_remapping/interp_NCL_ESMF_CURV_to_CURV
# Localisation of the data to remap
DATADIR=/data/scratch/globc/coquart/CVDP/CVDP-ncl-5.2.0/local_data/CERFACS-EPOC-LR/control/siconc
RUNDIR1=$HOMEDIR/wkdir_esmf_NCL_${srcgrid}_${tgtgrid}_${interp_method}_ocean
RUNDIR2=$HOMEDIR/wkdir_esmf_NCL_${srcgrid}_${tgtgrid}_${interp_method}_land
############################# END USER SECTION ########################################################
# Remapping using ESMF with NCL (default) : land is masked
[ -d $RUNDIR1 ] || mkdir $RUNDIR1
cp $HOMEDIR/interp_esmf_CURV_to_CURV_one_step_ocean.ncl $RUNDIR1/interp_esmf_CURV_to_CURV_one_step_ocean.ncl
cp $GRIDDIR/grid_msk_area_${srcgrid}.nc $RUNDIR1/grid_msk_area_${srcgrid}.nc
cp $GRIDDIR/grid_msk_area_${tgtgrid}.nc $RUNDIR1/grid_msk_area_${tgtgrid}.nc
ln -sf $DATADIR/$datafileO $RUNDIR1/.
#
cd $RUNDIR1
#
(time ncl 'JOBO="'${JOBO}'"' 'JOBO1="'${JOBO1}'"' 'interp_method="'${interp_method}'"' 'srcgrid="'${srcgrid}'"' 'tgtgrid="'${tgtgrid}'"' 'srcperiodic="'${srcperiodic}'"' 'tgtperiodic="'${tgtperiodic}'"' 'datafile="'${datafileO}'"' 'varin="'${var_inO}'"' interp_esmf_CURV_to_CURV_one_step_ocean.ncl) > log_ocean 2>&1

# Remapping using ESMF with NCL from land to land : ocea or atmo is masked
if [ $land_interp == 1 ]; then
    [ -d $RUNDIR2 ] || mkdir $RUNDIR2
    cp $HOMEDIR/interp_esmf_CURV_to_CURV_one_step_land.ncl $RUNDIR2/interp_esmf_CURV_to_CURV_one_step_land.ncl
    cp $GRIDDIR/grid_msk_area_${srcgrid}.nc $RUNDIR2/grid_msk_area_${srcgrid}.nc
    cp $GRIDDIR/grid_msk_area_${tgtgrid}.nc $RUNDIR2/grid_msk_area_${tgtgrid}.nc
    ln -sf $DATADIR/$datafileL $RUNDIR2/.
#
    cd $RUNDIR2
#
    (time ncl 'JOBL="'${JOBL}'"' 'JOBL1="'${JOBL1}'"' 'interp_method="'${interp_method}'"' 'srcgrid="'${srcgrid}'"' 'tgtgrid="'${tgtgrid}'"' 'srcperiodic="'${srcperiodic}'"' 'tgtperiodic="'${tgtperiodic}'"' 'datafile="'${datafileL}'"' 'varin="'${var_inL}'"' interp_esmf_CURV_to_CURV_one_step_land.ncl) > log_land 2>&1
#
# Combine remapping on sea (atmo) points and on land points
# cdo add : add values whenever there are missing values or not
# cdo ensmean : take only the not missing value (thanks to J. Boe)
#    cdo ensmean fldou_ana_ocean.nc fldou_ana_land.nc fldou_ana_final.nc
     cp $RUNDIR1/fldou_interp_ocean.nc $RUNDIR2/.
     cdo add fldou_interp_ocean.nc fldou_interp_land.nc temp.nc
     ncks -A -h -v field_ou_interp temp.nc fldou_final.nc
fi

