#!/bin/ksh
#
# Script launching NCL programs to calculate the ramapping of an analytical function in one
# step with ESMF, using nearest option for target points that do not receive any value,
# to verify the quality of interpolation 
# Written by L. Coquart 01/08/2022
#
############################# USER SECTION ########################################################
## GRIDS: used in the name of the name of the working directory
# Name of the grids : OASIS nomenclature
# grid and mask : 4 characters for the name, and then _lon, _lat, _clo, _cla, _msk
# grid in double precision, mask as an integer
# By default in OASIS : mask = 1 on land and 0 on ocean when ocean is not masked
# ESMF used inversed convention. So when remapping from ocean to ocean ESMF uses
# 1-mask
#++++++++++++++++++++++++++++++++
## INTERPOLATION: bilinear, conserve, patch (2nd order), neareststod
# Remapping (done with ESMF in an NCL program see below), used in 
# the name of the working directory
# Corners needed for conserv interpolation
#++++++++++++++++++++++++++++++++
# Remap with land masked by default
# If land_interp=1 remap also from land to land with ocean masked
# and combined fields at the end with cdo
######################################################################
HOMEDIR=`pwd`
land_interp=0
srcgrid=era5
tgtgrid=best
# Are the weights already calculated for ocean ? (put get_wgt and/or get_wget1 if not else use_wgt and use_wgt1)
JOBO="get_wgt"
JOBO1="get_wgt1"
# Are the grids regional (region) or periodical (global) ?
srcperiodic=global
tgtperiodic=global
#interp_method=bilinear
#interp_method=conserve
interp_method=patch
#interp_method=neareststod
var_in=tas
datafile=tas_1d_2003_ERA5.nc
# Localisation of the grids
GRIDDIR=/data/scratch/globc/$USER/use_ncl_with_esmf_for_remapping/interp_NCL_ESMF_CURV_to_CURV
# Localisation of the data to remap
DATADIR=/data/scratch/globc/dcom/globc_obs/ERA5/tas_1d
RUNDIR1=$HOMEDIR/wkdir_esmf_NCL_${srcgrid}_${tgtgrid}_${interp_method}_ocean
############################# END USER SECTION ########################################################
# Remapping using ESMF with NCL (default) : land is masked
[ -d $RUNDIR1 ] || mkdir $RUNDIR1
cp $HOMEDIR/interp_esmf_CURV_to_CURV_one_step_nolsmsk.ncl $RUNDIR1/interp_esmf_CURV_to_CURV_one_step_nolsmsk.ncl
cp $GRIDDIR/grid_msk_area_${srcgrid}.nc $RUNDIR1/grid_msk_area_${srcgrid}.nc
cp $GRIDDIR/grid_msk_area_${tgtgrid}.nc $RUNDIR1/grid_msk_area_${tgtgrid}.nc
ln -sf $DATADIR/$datafile $RUNDIR1/.
#
cd $RUNDIR1
#
(time ncl 'JOB="'${JOB}'"' 'interp_method="'${interp_method}'"' 'srcgrid="'${srcgrid}'"' 'tgtgrid="'${tgtgrid}'"' 'srcperiodic="'${srcperiodic}'"' 'tgtperiodic="'${tgtperiodic}'"' 'datafile="'${datafile}'"' 'varin="'${var_in}'"' interp_esmf_CURV_to_CURV_one_step_nolsmsk.ncl) > log_ocean 2>&1

